# Exercice_base_de_donnees

Ce repository contient un exercice en 6 parties :
* Partie 1 : créer une connexion à SQLite
* Partie 2 : créer une table
* Partie 3 : insérer des données dans la table
* Partie 4 : sélectionner des données dans la table
* Partie 5 : supprimer la table
* Partie 6 : même exercice avec PostGreSQL

Pour répondre aux exercices, vous devez travailler sur la branche de votre prénom. Pour cloner uniquement **votre branche**, tapez la commande suivante :
`git clone --branch <votre prenom> https://gitlab.com/simplon-dev-data/exercice_base_de_donnees.git`

Les fichiers de réponses doivent être nommés de la façon suivante pour pouvoir passer les tests :
* Exercice_1_reponse.py
* Exercice_2_reponse.py
* ...

Pour envoyer la réponse sur le serveur, vous tapez :
`git push origin <votre prenom>`
