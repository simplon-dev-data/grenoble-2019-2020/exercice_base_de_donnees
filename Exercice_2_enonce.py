#####################################
# EXERCICE BASE DE DONNEES PARTIE 2 #
#####################################
""" L'objectif de cette partie est 
de créer une base de données SQLite et les fonctions de base"""

# Importer le module sqlite3 de la librairie standard de Python
# CODE A AJOUTER

def connect_db(db_name="bdd.db"):
    # Créer un objet "connection" pour créer ou se connecter à la base
    # de données SQLite nommée "bdd.db"
    # CODE A AJOUTER

    # Pour exécuter du code SQL, il faut créer un curseur
    # Créer un objet "cursor"
    # CODE A AJOUTER

    return connection, cursor

def create_table(connection, cursor, table_name, dict_columns):
    # Créer la requête SQL
    # CODE A AJOUTER

    # Exécuter la requête SQL
    # CODE A AJOUTER

    # Commiter les modifications
    # CODE A AJOUTER

if __name__=="__main__":
    connection, cursor = connect_db()
    print("Creation de base de données : pas d'erreurs")
    create_table(connection, cursor, "villes",
        {"nom": "varchar(100)", 
        "code_postal":"varchar(100)",
        "population": "int"})
    print("Creation de la table : pas d'erreurs")

