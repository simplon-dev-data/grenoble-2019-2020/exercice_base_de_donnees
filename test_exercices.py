import pytest
import sqlite3
import psycopg2

@pytest.fixture
def connection():
    connection = sqlite3.connect(":memory:")
    yield connection
    connection.close()

try:
    import Exercice_1_reponse as ex1
    def test_exercice_1():
        conn, cur = ex1.connect_db()
        assert isinstance(conn, sqlite3.Connection)
        assert isinstance(cur, sqlite3.Cursor)
except:
    print("Fichier 1 absent")


try:
    import Exercice_2_reponse as ex2
    @pytest.mark.parametrize("table_name", ["villes", "test"])
    @pytest.mark.parametrize("dict_columns", [
                                            {"nom": "varchar(100)", 
                                                "code_postal":"varchar(100)",
                                                "population": "int"},
                                            {"test1": "float", 
                                                "test2":"int"},
                                                ])
    def test_exercice_2(connection, table_name, dict_columns):
        cursor = connection.cursor()
        ex2.create_table(connection, cursor, table_name, dict_columns)
        sql = f"PRAGMA table_info({table_name})"
        cursor.execute(sql)
        for item, col in zip(dict_columns.items(), cursor.fetchall()):
            assert item[0] in col
            assert item[1] in col
except:
    print("Fichier 2 absent")

try:
    import Exercice_3_reponse as ex3
    @pytest.mark.parametrize("table_name", ["villes", "test"])
    @pytest.mark.parametrize("dict_columns", [
                                            {"nom": "varchar(100)", 
                                                "code_postal":"varchar(100)",
                                                "population": "int"},
                                                ])
    @pytest.mark.parametrize("rows", [
                                            [
                                                ("Paris", "75000", 2000000),
                                                ("Marseille", "13000", 860000),
                                                ("Lyon", "69000", 516000),
                                            ],
                                            [
                                                ("Paris", "75000", 2000000),
                                                ("Marseille", "13000", 860000),
                                            ],
                                                ])
    def test_exercice_3(connection, table_name, dict_columns, rows):
        cursor = connection.cursor()
        ex2.create_table(connection, cursor, table_name, dict_columns)
        ex3.insert_data(connection, cursor, table_name, rows)
        sql = f"SELECT * FROM {table_name}"
        cursor.execute(sql)
        result = cursor.fetchall()
        assert len(result)==len(rows)
        for row, elt in zip(rows, result):
            assert row==elt
except:
    print("Fichier 3 absent")

try:
    import Exercice_4_reponse as ex4
    @pytest.mark.parametrize("table_name", ["villes", "test"])
    @pytest.mark.parametrize("dict_columns", [
                                            {"nom": "varchar(100)", 
                                                "code_postal":"varchar(100)",
                                                "population": "int"},
                                                ])
    @pytest.mark.parametrize("rows", [
                                            [
                                                ("Paris", "75000", 2000000),
                                                ("Marseille", "13000", 860000),
                                                ("Lyon", "69000", 516000),
                                            ],
                                            [
                                                ("Paris", "75000", 2000000),
                                                ("Marseille", "13000", 860000),
                                            ],
                                                ])
    def test_exercice_4(connection, table_name, dict_columns, rows):
        cursor = connection.cursor()
        ex2.create_table(connection, cursor, table_name, dict_columns)
        ex3.insert_data(connection, cursor, table_name, rows)
        responses = ex4.select_all_data(connection, cursor, table_name)
        assert len(responses)==len(rows)
        for response, row in zip(responses, rows):
            assert response==row
except:
    print("Fichier 4 absent")

try:
    import Exercice_5_reponse as ex5
    @pytest.mark.parametrize("table_name", ["villes", "test"])
    @pytest.mark.parametrize("dict_columns", [
                                            {"nom": "varchar(100)", 
                                                "code_postal":"varchar(100)",
                                                "population": "int"},
                                            {"test1": "float", 
                                                "test2":"int"},
                                                ])
    def test_exercice_5(connection, table_name, dict_columns):
        cursor = connection.cursor()
        ex2.create_table(connection, cursor, table_name, dict_columns)
        sql = f"PRAGMA table_info({table_name})"
        cursor.execute(sql)
        for item, col in zip(dict_columns.items(), cursor.fetchall()):
            assert item[0] in col
            assert item[1] in col
        ex5.drop_table(connection, cursor, table_name)
        sql = f"PRAGMA table_info({table_name})"
        cursor.execute(sql)
        assert cursor.fetchall()==[]
except:
    print("Fichier 5 absent")

try:
    import Exercice_6_reponse as ex6
    def test_exercice_6():
        connection, cursor = ex6.connect_db()
        ex6.create_table(connection, cursor, "villes",
            {"nom": "varchar(100)", 
            "code_postal":"varchar(100)",
            "population": "int"})
        data = [
            ("Paris", "75000", 2000000),
            ("Marseille", "13000", 860000),
            ("Lyon", "69000", 516000),
        ]
        ex6.insert_data(connection, cursor, "villes", data)
        responses = ex6.select_all_data(connection, cursor, "villes")
        assert len(responses)==len(data)
        for response, row in zip(responses, data):
            assert response==row
        ex6.drop_table(connection, cursor, "villes")
        with pytest.raises(psycopg2.errors.UndefinedTable):
            responses = ex6.select_all_data(connection, cursor, "villes")
except:
    print("Fichier 6 absent")